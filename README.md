# scl-plugin

> siemens scl (structured control language) integration for vim

## Requirements
- [vundle](https://github.com/VundleVim/Vundle.vim) plugin manager

### Install

Add this to your '.vimrc':

```Plugin 'https://gitlab.com/dasvh/scl-plugin'```

