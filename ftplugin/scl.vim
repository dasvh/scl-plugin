" ftplugin/scl.vim
if exists("b:did_ftplugin")
    finish
endif

let b:did_ftplugin = 1

if exists("loaded_matchit")
    let b:match_ignorecase = 1
endif
