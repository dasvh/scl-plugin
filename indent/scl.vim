" Vim indent file
" Language:    SCL (structured control language)
" Maintainer:  Dario Häfeli <dario.haefeli@wisol.ch>
" Created:     2017 Sep 29
" Last Change: 2022 Jan 04
"
if exists("b:did_indent")
    finish
endif
let b:did_indent = 1

setlocal indentexpr=GetSCLIndent(v:lnum)
setlocal indentkeys+==end_for,end_if,end_var,end_struct,end_while,end_repeat,end_region
setlocal indentkeys+==type,datablock,function,function_block

if exists("*GetSCLIndent")
    finish
endif


function! s:GetPrevNonCommentLineNum( line_num )

" skip lines that are commented out
    let SKIP_LINES = '^\s*\( \	((\*\)\	|\  (\*\\)  \|	\(\*)\)   )' "\|\({\)\)'

    let nline = a:line_num
    while nline > 0
        let nline = prevnonblank(nline-1)
        if getline(nline) !~? SKIP_LINES
            break
        endif
    endwhile

    return nline
endfunction

function! s:PurifyCode( line_num )
" Strip any trailing comments and whitespace
    let pureline = 'TODO'
    return pureline
endfunction

function! GetSCLIndent( line_num )

" Line 0 always goes at column 0
    if a:line_num == 0
        return 0
    endif

    let this_codeline = getline( a:line_num )

	if this_codeline =~ '^\s*\(type\|datablock\|function\|function_block\)\>'
	    return 0
	endif

    let prev_codeline_num = s:GetPrevNonCommentLineNum( a:line_num )
    let prev_codeline = getline( prev_codeline_num )
	let indnt = indent( prev_codeline_num )

	if prev_codeline =~ '^\s*region\>'
        return indnt + shiftwidth()
    endif

    " If the PREVIOUS LINE ended with these items, indent
    if prev_codeline =~ '\<\(\|then\|else\|do\|of\|repeat\)$' "|| prev_codeline =~ ':$'
	    return indnt + shiftwidth()
	endif

    if prev_codeline =~ '\<\(\|var_input\|var_output\|var_in_out\|var_temp\|var\|retain\|begin\|struct\)$'
        return indnt + shiftwidth()
    endif

	"set end_ on same indent
    "if this_codeline =~ '^\s*\(end_if\|end_for\|end_var\|end_struct\|end_while\|end_repeat\)\>'
    if this_codeline =~ '\<\(\|end_if\|end_for\|end_var\|end_struct\|end_while\|end_repeat\|end_region\)$'
        return indnt - shiftwidth()
    endif

    " DECREASE INDENT
    " Lines starting with "else", but not following line ending with
    " "end_if;".
    if this_codeline =~ '^\s*\(else\|elsif\)\>' && prev_codeline !~ '\<end_if;$'
	    return indnt - shiftwidth()
    endif

	" Lines after a single-statement branch/loop.
        " Two lines before ended in "then", "else", or "do"
        "let prev2_codeline_num = s:GetPrevNonCommentLineNum( prev_codeline_num )
        "let prev2_codeline = getline( prev2_codeline_num )
        "if prev2_codeline =~ '\<\(\|then\|else\|do\)$' "&& prev_codeline !~ '\<begin$'
	    " If the next code line after a single statement branch/loop
            " starts with "end", "except" or "finally", we need an
            " additional unindentation.
"            if this_codeline =~ '^\s*\(elsif\|except\|finally\|\)$'
"		" Note that we don't return from here.
"		return indnt - shiftwidth() - shiftwidth()
"	    endif
	 "   return indnt - shiftwidth()
	"endif

	return indnt
endfunction
