" Vim syntax file
" Language:	SCL (structured control language)
" Version: 0.1
" Last Change:	2022/01/4 16:06:45
" Maintainer:  Dario Häfeli <dario.haefeli@wisol.ch>

" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif


syn case ignore
syn sync lines=250

syn keyword sclKeywords1    AND ANY ARRAY AT BEGIN BLOCK_DB BLOCK_FB BLOCK_FC BLOCK_SDB BOOL BY BYTE CASE CHAR CONST CONTINUE COUNTER DATA_BLOCK DATE DATE_AND_TIME DINT DIV DO DT DWORD ELSE ELSIF EN END_CASE END_CONST END_DATA_BLOCK END_FOR END_FUNCTION END_FUNCTION_BLOCK END_IF END_LABEL END_ORGANIZATION_BLOCK END_REPEAT END_STRUCT END_TYPE END_VAR END_WHILE ENO EXIT FOR FUNCTION FUNCTION_BLOCK GOTO IF INT LABEL MOD NIL NOT OF OK OR ORGANIZATION_BLOCK POINTER PROGRAM END_PROGRAM REAL REPEAT RET_VAL RETURN S5TIME STRING STRUCT THEN TIME TIME_OF_DAY TIMER TO TOD TYPE UNTIL VAR VAR_IN_OUT VAR_INPUT VAR_OUTPUT VAR_TEMP VOID WHILE WORD XOR
syn keyword sclKeywords2    ABS SQR SQRT EXP EXPD LN LOG ACOS ASIN ATAN COS SIN TAN ROL ROR SHL SHR LEN CONCAT LEFT RIGHT MID INSERT DELETE REPLACE FIND EQ_STRNG NE_STRNG GE_STRNG LE_STRNG GT_STRNG LT_STRNG INT_TO_STRING STRING_TO_INT DINT_TO_STRING STRING_TO_DINT REAL_TO_STRING STRING_TO_REAL SEL MAX MIN LIMIT MUX BCD_TO_INT INT_TO_BCD BYTE_TO_INT INT_TO_BYTE BCD_TO_DINT BCD_TO_INT BLOCK_DB_TO_WORD BOOL_TO_DINT BOOL_TO_INT BYTE_TO_BOOL BYTE_TO_CHAR BYTE_TO_DINT BYTE_TO_INT CHAR_TO_BYTE CHAR_TO_INT DATE_TO_DINT DINT_TO_BCD DINT_TO_BCD_DWORD DINT_TO_BOOL DINT_TO_BYTE DINT_TO_DATE DINT_TO_DWORD DINT_TO_INT DINT_TO_STRING DINT_TO_TIME DINT_TO_TOD DINT_TO_WORD DWORD_BCD_TO_DINT DWORD_TO_BOOL DWORD_TO_BYTE DWORD_TO_DINT DWORD_TO_INT DWORD_TO_REAL DWORD_TO_WORD INT_TO_BCD INT_TO_BCD_WORD INT_TO_BOOL INT_TO_BYTE INT_TO_CHAR INT_TO_DWORD INT_TO_STRING INT_TO_WORD REAL_TO_DINT REAL_TO_DWORD REAL_TO_INT REAL_TO_STRING STRING_TO_CHAR STRING_TO_DINT STRING_TO_INT STRING_TO_REAL TIME_TO_DINT TOD_TO_DINT WORD_BCD_TO_INT WORD_TO_BLOCK_DB WORD_TO_BOOL WORD_TO_BYTE WORD_TO_DINT WORD_TO_INT ROUND TRUNC TIME_TO_S5TIME S5TIME_TO_TIME
syn keyword sclKeywords3    TITLE VERSION KNOW_HOW_PROTECT AUTHOR NAME FAMILY
syn keyword sclKeywords4    A B Q AB QB AD QD AW QW AX QX D D DB DD DW DX E I EB IB ED ID EW IW EX IX M MB MD MW MX PAB PQB PAD PQD PAW PQW PEB PIB PED PID PEW PIW FB FC OB SDB SFC SFB T UDT Z C
syn keyword sclRegion   region end_region
syn keyword sclOperator	MOD DIV NOT AND OR XOR
syn keyword sclConstant	true false
syn match sclUserSpecialCharacter display "[+-<>=]"

syn region sclStringL matchgroup=sclStringL start="'" end="'" contains=sclString,sclCharacter,sclNumbersCom

if exists("scl_comment_strings")
   " A comment can contain cString, cCharacter and cNumber.
   " But a "*/" inside a cString in a cComment DOES end the comment!  So we
   " need to use a special type of cString: cCommentString, which also ends on
   " "*/", and sees a "*" at the start of the line as comment again.
   " Unfortunately this doesn't very well work for // type of comments :-(
   syn match    sclCommentSkip    contained "^\s*\*\($\|\s\+\)"
   syn region   sclCommentString     contained start=+L\=\\\@<!"+ skip=+\\\\\|\\"+ end=+"+ end=+\*/+me=s-1 contains=sclSpecial,sclCommentSkip
   syn region   sclComment2String    contained start=+L\=\\\@<!"+ skip=+\\\\\|\\"+ end=+"+ end="$" contains=sclSpecial
   syn region   sclCommentL start="//" skip="\\$" end="$" keepend contains=@sclCommentGroup,sclComment2String,sclCharacter,sclNumbersCom,sclSpaceError,@Spell
   if exists("scl_no_comment_fold")
     " Use "extend" here to have preprocessor lines not terminate halfway a
     " comment.
     syn region sclComment matchgroup=sclCommentStart start="(\*" end="\*)" contains=@sclCommentGroup,sclCommentStartError,sclCommentString,sclCharacter,sclNumbersCom,sclSpaceError,@Spell extend
   else
     syn region sclComment matchgroup=sclCommentStart start="(\*" end="\*)" contains=@sclCommentGroup,sclCommentStartError,sclCommentString,sclCharacter,sclNumbersCom,sclSpaceError,@Spell fold extend
   endif
 else
   syn region    sclCommentL       start="//" skip="\\$" end="$" keepend contains=@sclCommentGroup,sclSpaceError,@Spell
   if exists("c_no_comment_fold")
     syn region  sclComment        matchgroup=sclCommentStart start="(\*" end="\*)" contains=@sclCommentGroup,sclCommentStartError,sclSpaceError,@Spell extend
   else
     syn region  sclComment        matchgroup=sclCommentStart start="(\*" end="\*)" contains=@sclCommentGroup,sclCommentStartError,sclSpaceError,@Spell fold extend
   endif
 endif
 " keep a // comment separately, it terminates a preproc. conditional
 syn match       sclCommentError   display "\*/"
 syn match       sclCommentStartError display "/\*"me=e-1 contained

"integer number, or floating point number without a dot and with "f".
 syn case ignore
 syn match       sclNumbers        display transparent "\<\d\|\.\d" contains=sclNumber,sclFloat,sclOctalError,sclOctal
 " Same, but without octal error (for comments)
 syn match       sclNumbersCom     display contained transparent "\<\d\|\.\d" contains=sclNumber,sclFloat,sclOctal
 syn match       sclNumber         display contained "\d\+\(u\=l\{0,2}\|ll\=u\)\>"
 "hex number
 syn match       sclNumber         display contained "0x\x\+\(u\=l\{0,2}\|ll\=u\)\>"
 " Flag the first zero of an octal number as something special
 syn match       sclOctal          display contained "0\o\+\(u\=l\{0,2}\|ll\=u\)\>" contains=sclOctalZero
 syn match       sclOctalZero      display contained "\<0"
 syn match       sclFloat          display contained "\d\+f"
 "floating point number, with dot, optional exponent
 syn match       sclFloat          display contained "\d\+\.\d*\(e[-+]\=\d\+\)\=[fl]\="
 "floating point number, starting with a dot, optional exponent
 syn match       sclFloat          display contained "\.\d\+\(e[-+]\=\d\+\)\=[fl]\=\>"
 "floating point number, without dot, with exponent
 syn match       sclFloat          display contained "\d\+e[-+]\=\d\+[fl]\=\>"

hi def link sclCommentString	    sclString
hi def link sclComment2String	    sclString
hi def link sclCommentL		    sclComment
hi def link sclCommentStart	    sclComment
hi def link sclCommentSkip	    sclComment
hi def link sclComment		    Comment
hi def link sclStringL		    sclString
hi def link sclString		    String
hi def link sclCommentError	    sclError
hi def link sclCommentStartError    sclError
hi def link sclKeywords1	    Statement
hi def link sclKeywords2	    Operator
hi def link sclKeywords3	    Comment
hi def link sclKeywords4	    sclNumber
hi def link sclSpecial		    SpecialChar
hi def link sclCharacter	    Character
hi def link sclUserSpecialCharacter sclSpecial
hi def link sclSpaceError	    sclError
hi def link sclDelimiter	    Identifier
hi def link sclFloat		    Float
hi def link sclIdentifier	    sclSpecial
hi def link sclNumber		    Number
hi def link sclOctal		    Number
hi def link sclOctalZero	    PreProc
hi def link sclOperator		    Operator
hi def link sclError		    Error
hi def link sclConstant		    Constant
hi def link sclRegion               Tag

let b:current_syntax = "scl"

" vim: ts=8 sw=2
